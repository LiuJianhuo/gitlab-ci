@echo off
for /f %%i in ('git config remote.origin.url') do @set remote=%%i


set remote=%remote%
set siteSource=dist

echo %remote%

:: 发布分支
set branch=test-branch



:: 新建一个发布的目录
mkdir %branch%
cd %branch%
git init
git remote add --fetch origin %remote%
git checkout --orphan %branch%
git rm -rf .
xcopy ..\%siteSource%\. . /s /y

:: 把所有的文件添加到git
git add -A
git commit --allow-empty -m "deploy: build docs [ci skip]"
:: git push --force origin test-branch
